from werkzeug.security import check_password_hash
from models.employee_model import Employee

def authentiction(username,password):
    emp = Employee.query.filter_by(email=username).first()
    if emp and check_password_hash(emp.password,password):
        return emp

def identity(payload):
    user_id = payload['identity']

    return Employee.query.filter_by(id=user_id).first()