from flask import Flask
from flask_cors import CORS
from flask_jwt import JWT,jwt_required
from models.employee_model import Employee, db
import datetime

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI']="postgresql:///employee"
app.config['SECRET_KEY']="secretkey"
app.config['JWT_EXPIRATION_DELTA'] = datetime.timedelta(days=1)
cors = CORS(app, resources={r"/*/*": {"origins": r"*"}})

from utils.security import authentiction,identity
jwt = JWT(app,authentiction,identity)

with app.app_context():
    db.init_app(app)
    db.create_all()

if __name__ == "__main__":
    app.run(port=5007,debug=True)